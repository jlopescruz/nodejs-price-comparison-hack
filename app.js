/* Dependencies */
var globals     = require('./globals'),
    request     = require('request'),
    cheerio     = require('cheerio'),
    Firebase    = require('firebase'),
    colors      = require('colors'),
    nodemailer  = require('nodemailer');

/* Generic vars */
var myFirebaseRef = new Firebase('https://banus-wishlist.firebaseio.com/links-produtos'),
    produtos      = {},
    cont          = 0,
    contMax       = 0,
    keys          = [],
    novo_preco    = 0,
    transporter   = null,
    mailOptions   = null,
    firebaseKey   = 0,
    element       = '';

/* Constant vars */
var ENABLE_EMAIL  = false,
    TIMEOUT       = 10000;

var getElementWithValue = function(value, $) {
    return $("*:not(script):contains('" + value +"')").last();
};

myFirebaseRef.on('value', function(snapshot) {
    
    produtos = snapshot.val();
    
    for (product in produtos) {
        keys.push(product);
    }

    contMax = keys.length;

    firstLoad = false;

    setInterval(function() {
        request(produtos[keys[cont]].url, function(err, resp, body) {

            if (err) { return; }

            $ = cheerio.load(body);

            element = getElementWithValue(produtos[keys[cont]].preco_original, $);

            novo_preco = element.text();

            if (novo_preco !== '') {

                if (novo_preco === produtos[keys[cont]].preco_original) {
                    process.stdout.write("\u001b[2J\u001b[0;0H");
                    console.log(colors.green(produtos[keys[cont]].nome));
                    console.log('Preco continua igual - ' + produtos[keys[cont]].preco_original);
                } else {
                    /* Terminal notification */
                    process.stdout.write("\u001b[2J\u001b[0;0H");
                    console.log(colors.red(produtos[keys[cont]].nome));
                    console.log('Preco mudou!! - ' + novo_preco);

                    if (ENABLE_EMAIL) {
                        /* Send email */
                        transporter = nodemailer.createTransport({
                            service: 'Gmail',
                            auth: {
                                user: globals.config.gmail.email,
                                pass: globals.config.gmail.pass
                            }
                        });

                        mailOptions = {
                            from:     'App dos Banus - Price notification <appbanus@gmail.com>',
                            to:       'jlopes.cruz@gmail.com',
                            subject:  produtos[keys[cont]].nome + ' - preço mudou',
                            html:     '<h1>' + produtos[keys[cont]].nome + '</h1><p>Preço mudou para £' + novo_preco + '</p>'
                        };
                        
                        transporter.sendMail(mailOptions, function(error, info){
                        });
                    }

                    /* Update price value on firebase */
                    firebaseKey = keys[cont];
                    myFirebaseRef.child(firebaseKey).update({ preco_original:  novo_preco});
                }

            } else {
                process.stdout.write("\u001b[2J\u001b[0;0H");
                console.log(colors.red(produtos[keys[cont]].nome));
                console.log('Nao é possivel encontrar o produto');
            }

            cont++;

            if (cont >= contMax) {
                cont = 0;
            }
        });
    }, TIMEOUT);
});